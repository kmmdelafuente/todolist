package com.example.kristeldelafuente.todolist;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class AddList extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_list);
        setTitle("Add List");
    }

    public void saveNewList(View v) {
        EditText editTextNewList = (EditText) findViewById(R.id.editTextNewList);
        String listName = editTextNewList.getText().toString();

        FileOutputStream fos = null;

        try {
            fos = openFileOutput(listName, MODE_PRIVATE);
            fos.write(listName.getBytes());

            editTextNewList.getText().clear();

            Intent goToChecklist = new Intent();
            goToChecklist.setClass(this, ToDoChecklist.class);
            goToChecklist.putExtra("checklistName", listName);
            startActivity(goToChecklist);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (fos != null) {
                try {
                    fos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

    }
}
