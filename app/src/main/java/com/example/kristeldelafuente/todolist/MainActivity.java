package com.example.kristeldelafuente.todolist;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import java.io.File;
public class MainActivity extends AppCompatActivity {

    private List<String> fileList = new ArrayList<String>();
    private ListView toDoListCategories;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        toDoListCategories = (ListView) findViewById(R.id.toDoListCategories);
        File root = getFilesDir();
        ListDir(root);
    }

    void ListDir(File f) {
        File[] files = f.listFiles();
//        Log.d("KLOG", String.valueOf(files.length));
        fileList.clear();

        if (files != null) {
            for (File file : files) {
                fileList.add(file.getName());
            }
        }

        ListAdapter directoryList = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, fileList);
        toDoListCategories.setAdapter(directoryList);

        toDoListCategories.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent goToChecklist = new Intent();
                goToChecklist.setClass(MainActivity.this, ToDoChecklist.class);
                goToChecklist.putExtra("checklistName", fileList.get(position));
                startActivity(goToChecklist);
            }
        });
    }

    public void addList(View v) {
        Intent goToAddActivity = new Intent();
        goToAddActivity.setClass(this, AddList.class);
        startActivity(goToAddActivity);
    }
}
