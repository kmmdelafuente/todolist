package com.example.kristeldelafuente.todolist;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;

public class ToDoChecklist extends AppCompatActivity {

    private ListView listViewToDoChecklist;
    private String checklistName;
    private List<String> checklistItems = new ArrayList<String>();
    private List<String> checklistItemsStatus = new ArrayList<String>();
    private Button newChecklistItemBtn;
    private EditText newChecklistItem;
    private File file;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_to_do_checklist);

        Intent caller = getIntent();
        checklistName = caller.getStringExtra("checklistName");
        setTitle(checklistName);

        listViewToDoChecklist = (ListView) findViewById(R.id.listViewToDoChecklist);

        newChecklistItem = (EditText) findViewById(R.id.newChecklistItem);
        newChecklistItemBtn = (Button) findViewById(R.id.newChecklistItemBtn);

        getChecklistItems();

        listViewToDoChecklist.setOnItemClickListener(new android.widget.AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view,int position, long id) {
                String item = (String) listViewToDoChecklist.getItemAtPosition(position);
                checklistItemsStatus.set(position, item + (listViewToDoChecklist.isItemChecked(position) ? "--" : ""));

                updateChecklist();
            }
        });
    }

    protected void getChecklistItems() {
        try {
            file = new File(getFilesDir() + "/" + checklistName);

            FileReader fileReader = new FileReader(file);
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            String line;

            while ((line = bufferedReader.readLine()) != null) {
                checklistItems.add(getChecklistItemLabel(line));
                checklistItemsStatus.add(line);
            }
            fileReader.close();

            ListAdapter checklistItemsAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_multiple_choice, checklistItems);
            listViewToDoChecklist.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
            listViewToDoChecklist.setAdapter(checklistItemsAdapter);

            for (int i = 0; i < checklistItemsStatus.size(); i++) {
                if(isItemChecked(checklistItemsStatus.get(i))) {
                    listViewToDoChecklist.setItemChecked(i, true);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void addItem(View v) {
        String itemName = newChecklistItem.getText().toString();
        if(itemName != null) {
            checklistItems.add(itemName);
            checklistItemsStatus.add(itemName);
            updateChecklist();
            newChecklistItem.getText().clear();
        }
    }

    private String getChecklistItemLabel(String name) {
        if(name.substring(name.length() - 2).equals("--")) {
            return name.substring(0, name.length() - 2);
        }

        return name;
    }

    private boolean isItemChecked(String name) {
        return  name.substring(name.length() - 2).equals("--");
    }

    private void updateChecklist() {
        FileOutputStream fos = null;
        try {
            fos = openFileOutput(checklistName, MODE_PRIVATE);
            fos.write(String.join("\n", checklistItemsStatus).getBytes());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (fos != null) {
                try {
                    fos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        ((BaseAdapter) listViewToDoChecklist.getAdapter()).notifyDataSetChanged();
    }
}
